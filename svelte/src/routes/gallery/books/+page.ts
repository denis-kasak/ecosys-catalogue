import { BookController } from '$lib/requests';
import type { PageLoad } from './$types';

export const ssr = false;

export const load: PageLoad = ({ params }) => {
	return {
		books: BookController.getAll()
	};
};
