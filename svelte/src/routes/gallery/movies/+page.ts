import type { PageLoad } from './$types';
import { MovieController } from '$lib/requests';

export const ssr = false;

export const load: PageLoad = ({ params }) => {
	return {
		movies: MovieController.getAll()
	};
};
