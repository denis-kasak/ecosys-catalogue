import type { PageLoad } from './$types';
import { MovieController } from '$lib/requests';
import { Movie } from '$lib/components/movie-details/movie';

export const ssr = false;

export const load: PageLoad = async ({ params }) => {
	const movie = (await MovieController.queryMovie(params.movieID)).basics;
	console.log('preload title', movie.title);
	return {
		movie: Movie.constructFromTmdbMovie(movie)
	};
};
