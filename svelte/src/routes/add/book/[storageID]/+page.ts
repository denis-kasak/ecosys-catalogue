import type { PageLoad } from './$types';
import { StorageHandler } from '$lib/util';

export const ssr = false;

export const load: PageLoad = ({ params }) => {
	return {
		book: StorageHandler.getObject(params.storageID)
	};
};
