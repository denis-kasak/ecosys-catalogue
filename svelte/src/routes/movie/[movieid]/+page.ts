import { MovieController } from '$lib/requests';
import type { PageLoad } from './$types';

export const ssr = false;

export const load: PageLoad = async ({ params }) => {
	return {
		movie: await MovieController.getOne(params.movieid)
	};
};
