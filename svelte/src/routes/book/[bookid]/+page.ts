import { BookController } from '$lib/requests';
import type { PageLoad } from './$types';

export const ssr = false;

export const load: PageLoad = async ({ params }) => {
	return {
		book: await BookController.getOne(params.bookid)
	};
};
