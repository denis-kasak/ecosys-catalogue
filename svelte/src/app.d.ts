declare namespace App {
	import type { BookSource, Feedback, BookStatus, MovieStatus } from '$lib/enum';
	interface Media {
		id: string;
		source: MediaSource;
		createdAt: number;
		updatedAt: number;
		userInfo?: Impression;
		thumbnail: string;
		nsfw: boolean;
		title: string;
		subtitle: string;
		status?: string;
		description: string;
	}
	interface Impression {
		feedback: BookFeedback;
		rating: number;
		review: string;
	}
	interface ImageLinks {}

	interface MediaSource {
		name: BookSource;
		id: string;
		selfLink: string;
	}

	declare namespace Movies {
		interface Movie extends Media {
			status: MovieStatus;
			movieInfo: {
				genres: string[];
				originalLanguage: string;
				originalTitle: string;
				productionCompanies: string[];
				productionCountries: string[];
				releaseDate: string;
				runtime: number;
				spokenLanguages: string[];
			};
		}
	}

	declare namespace Books {
		interface IndustryIdentifier {
			type?: string;
			identifier?: string;
		}
		interface Query {
			volumeid: string | undefined;
			query: string | undefined;
			intitle: string | undefined;
			inauthor: string | undefined;
			inpublisher: string | undefined;
			subject: string | undefined;
			isbn: string | undefined;
			lccn: string | undefined;
			oclc: string | undefined;
		}

		interface Book extends Media {
			status: BookStatus;
			volumeInfo: {
				authors: string[];
				categories: string[];
				industryIdentifiers: IndustryIdentifier[];
				language: string;
				pageCount: number;
				publisher: string;
				publishedDate: string;
			};
		}
	}
}
