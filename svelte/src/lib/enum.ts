export enum BookStatus {
	WANT_TO_READ = 'want to read',
	READING = 'reading',
	READ = 'read'
}

export enum Feedback {
	LIKE = 'like',
	DISLIKE = 'dislike',
	NONE = 'none'
}

export enum BookSource {
	GOOGLE_BOOKS = 'Google Books API',
	CUSTOM = 'custom'
}

export enum EditMode {
	EDIT = 'edit',
	CREATE = 'create',
	VIEW = 'view'
}

export enum MovieSource {
	TMDB = 'The Movie Database',
	CUSTOM = 'custom'
}

export enum MovieStatus {
	WANT_TO_WATCH = 'want to watch',
	WATCHING = 'watching',
	WATCHED = 'watched',
	ABANDONED = 'abandoned'
}
