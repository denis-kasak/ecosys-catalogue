import type google from 'googleapis';
const BACKEND_URL = 'http://localhost:5002';
import type {
	MovieResultsResponse,
	SearchMovieRequest,
	MovieResponse,
	CreditsResponse,
	MovieImagesResponse
} from 'moviedb-promise';

export class BookController {
	static async deleteBook(id: string) {
		const response = await fetch(`${BACKEND_URL}/api/books/${id}`, { method: 'DELETE' });
		if (!response.ok) throw new Error(response.statusText);
	}
	static async queryBooks(query: App.Books.Query): Promise<google.books_v1.Schema$Volume[]> {
		const searchParams = new URLSearchParams({
			volumeid: query.volumeid || '',
			query: query.query || '',
			intitle: query.intitle || '',
			inauthor: query.inauthor || '',
			inpublisher: query.inpublisher || '',
			subject: query.subject || '',
			isbn: query.isbn || '',
			lccn: query.lccn || '',
			oclc: query.oclc || ''
		});

		const response = await fetch(`${BACKEND_URL}/api/books/extract?${searchParams}`);
		if (!response.ok) throw new Error(response.statusText);
		const json = await response.json();
		return json as google.books_v1.Schema$Volume[];
	}

	static async saveBook(book: App.Books.Book): Promise<App.Books.Book> {
		const response = await fetch(`${BACKEND_URL}/api/books`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(book)
		});
		if (!response.ok) throw new Error(response.statusText);
		const json = await response.json();
		return json as App.Books.Book;
	}

	static async updateBook(book: App.Books.Book): Promise<App.Books.Book> {
		const response = await fetch(`${BACKEND_URL}/api/books/${book.id}`, {
			method: 'PUT',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(book)
		});
		if (!response.ok) throw new Error(response.statusText);
		const json = await response.json();
		return json as App.Books.Book;
	}

	static async getOne(id: string) {
		const response = await fetch(`${BACKEND_URL}/api/books/${id}`);
		if (!response.ok) throw new Error(response.statusText);
		const json = await response.json();
		return json as App.Books.Book;
	}

	static async getAll() {
		const response = await fetch(`${BACKEND_URL}/api/books`);
		if (!response.ok) throw new Error(response.statusText);
		const json = await response.json();
		return json as App.Books.Book[];
	}
}

export class MovieController {
	static async saveMovie(movie: App.Movies.Movie) {
		const response = await fetch(`${BACKEND_URL}/api/movies`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(movie)
		});
		if (!response.ok) throw new Error(response.statusText);
		const json = await response.json();
		return json as App.Movies.Movie;
	}
	static async queryFullMovie(id: string) {
		const searchParams = new URLSearchParams({
			id
		});

		const response = await fetch(`${BACKEND_URL}/api/movies/extractfull?${searchParams}`);
		if (!response.ok) throw new Error(response.statusText);
		const json = await response.json();
		return json as {
			basics: MovieResponse;
			credits: CreditsResponse;
			images: MovieImagesResponse;
		};
	}
	static async queryMovies(query: SearchMovieRequest): Promise<MovieResultsResponse> {
		const searchParams = new URLSearchParams({
			query: query.query || '',
			include_adult: query.include_adult?.toString() || '',
			region: query.region || '',
			year: query.year?.toString() || '',
			language: query.language || ''
		});

		const response = await fetch(`${BACKEND_URL}/api/movies/extract?${searchParams}`);
		if (!response.ok) throw new Error(response.statusText);
		const json = await response.json();
		return json as MovieResultsResponse;
	}

	static async queryMovie(id: string | number) {
		const searchParams = new URLSearchParams({
			id: id.toString()
		});
		const response = await fetch(`${BACKEND_URL}/api/movies/extract?${searchParams}`);
		if (!response.ok) throw new Error(response.statusText);
		const json = await response.json();
		return json as {
			basics: MovieResponse;
			credits: CreditsResponse;
			images: MovieImagesResponse;
		};
	}

	static async updateMovie(movie: App.Movies.Movie): Promise<App.Movies.Movie> {
		const response = await fetch(`${BACKEND_URL}/api/movies/${movie.id}`, {
			method: 'PUT',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(movie)
		});
		if (!response.ok) throw new Error(response.statusText);
		const json = await response.json();
		return json as App.Movies.Movie;
	}

	static async getOne(id: string) {
		const response = await fetch(`${BACKEND_URL}/api/movies/${id}`);
		if (!response.ok) throw new Error(response.statusText);
		const json = await response.json();
		return json as App.Movies.Movie;
	}

	static async getAll() {
		const response = await fetch(`${BACKEND_URL}/api/movies`);
		if (!response.ok) throw new Error(response.statusText);
		const json = await response.json();
		return json as App.Movies.Movie[];
	}

	static async deleteMovie(id: string) {
		const response = await fetch(`${BACKEND_URL}/api/movies/${id}`, { method: 'DELETE' });
		if (!response.ok) throw new Error(response.statusText);
	}
}
