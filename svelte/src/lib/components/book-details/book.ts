import { Feedback, BookSource, BookStatus } from '$lib/enum';
import type google from 'googleapis';

export class Book implements App.Books.Book {
	id = '';
	source = {
		name: BookSource.CUSTOM,
		id: '',
		selfLink: ''
	};
	createdAt = Date.now();
	updatedAt = Date.now();
	userInfo = {
		status: BookStatus.READ,
		feedback: Feedback.NONE,
		rating: 0,
		review: ''
	};
	status = BookStatus.READ;
	thumbnail = '';
	nsfw = false;
	title = '';
	subtitle = '';
	description = '';
	volumeInfo = {
		authors: [],
		categories: [],
		industryIdentifiers: [],
		language: '',
		pageCount: 0,
		publisher: '',
		publishedDate: ''
	};

	constructor(book: App.Books.Book) {
		Object.assign(this, book);
	}

	static constructFromGoogleBook(result: google.books_v1.Schema$Volume) {
		let book: App.Books.Book = {
			id: '',
			source: {
				name: BookSource.GOOGLE_BOOKS,
				id: result.id || '',
				selfLink: result.selfLink || ''
			},
			status: BookStatus.READ,
			createdAt: Date.now(),
			updatedAt: Date.now(),
			thumbnail: result.volumeInfo?.imageLinks?.thumbnail || '',
			nsfw: false,
			title: result.volumeInfo?.title || '',
			subtitle: result.volumeInfo?.subtitle || '',
			description: result.volumeInfo?.description || '',
			volumeInfo: {
				authors: result.volumeInfo?.authors || [],
				categories: result.volumeInfo?.categories || [],
				industryIdentifiers: result.volumeInfo?.industryIdentifiers || [],
				language: result.volumeInfo?.language || '',
				pageCount: result.volumeInfo?.pageCount || 0,
				publisher: result.volumeInfo?.publisher || '',
				publishedDate: result.volumeInfo?.publishedDate || ''
			}
		};
		return new Book(book);
	}
}
