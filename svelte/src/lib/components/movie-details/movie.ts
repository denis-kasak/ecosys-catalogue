import { Feedback, MovieSource, MovieStatus } from '$lib/enum';
import type { MovieResponse } from 'moviedb-promise';

export class Movie implements App.Movies.Movie {
	id = '';
	source = {
		name: MovieSource.CUSTOM,
		id: '',
		selfLink: ''
	};
	createdAt = Date.now();
	updatedAt = Date.now();
	userInfo = {
		feedback: Feedback.NONE,
		rating: 0,
		review: ''
	};
	status = MovieStatus.WATCHED;
	thumbnail = '';
	nsfw = false;
	title = '';
	subtitle = '';
	description = '';
	movieInfo = {
		genres: [],
		originalLanguage: '',
		originalTitle: '',
		productionCompanies: [],
		productionCountries: [],
		releaseDate: '',
		runtime: 0,
		spokenLanguages: []
	};

	constructor(movie: App.Movies.Movie) {
		Object.assign(this, movie);
	}

	static constructFromTmdbMovie(result: MovieResponse) {
		let movie: App.Movies.Movie = {
			id: '',
			source: {
				name: MovieSource.TMDB,
				id: result.id?.toString() || '',
				selfLink: ''
			},
			status: MovieStatus.WATCHED,
			createdAt: Date.now(),
			updatedAt: Date.now(),
			thumbnail: result.poster_path
				? `https://image.tmdb.org/t/p/original/${result.poster_path}`
				: '',
			nsfw: result.adult || false,
			title: result.title || '',
			subtitle: '',
			description: result.overview || '',
			movieInfo: {
				originalLanguage: result.original_language || '',
				originalTitle: result.original_title || '',
				productionCompanies:
					result.production_companies?.map((company) => {
						return company.name || '';
					}) || [],
				productionCountries:
					result.production_countries?.map((country) => {
						return country.name || '';
					}) || [],
				releaseDate: result.release_date || '',
				runtime: result.runtime || 0,
				spokenLanguages:
					result.spoken_languages?.map((language) => {
						return language.name || '';
					}) || [],
				genres:
					result.genres?.map((genre) => {
						return genre.id?.toString() || '';
					}) || []
			}
		};
		return new Movie(movie);
	}
}
