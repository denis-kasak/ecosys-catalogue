import type { TreeViewNode } from '@skeletonlabs/skeleton';

export function countProperties(obj: any): number {
	if (typeof obj !== 'object' || obj === null) return 0; // Not an object or is null

	let count = 0;
	for (let key in obj) {
		if (obj.hasOwnProperty(key)) {
			count++;

			// Recurse into the value if it's an object
			if (typeof obj[key] === 'object' && obj[key] !== null) {
				count += countProperties(obj[key]);
			}
		}
	}

	return count;
}

export function constructTreeviewNodes(obj: any): TreeViewNode[] | undefined {
	if (typeof obj !== 'object' || obj === null) return undefined; // Not an object or is null

	let nodes: TreeViewNode[] = [];
	for (let key in obj) {
		if (obj.hasOwnProperty(key)) {
			let node: TreeViewNode = {
				id: key,
				content: obj[key],
				children: []
			};

			// Recurse into the value if it's an object
			if (typeof obj[key] === 'object' && obj[key] !== null) {
				node.children = constructTreeviewNodes(obj[key]);
			}

			nodes.push(node);
		}
	}

	return nodes;
}

export class StorageHandler {
	static createObject(obj: any) {
		const id = Date.now().toString();
		localStorage.setItem(id, JSON.stringify(obj));
		return id;
	}

	static popObject(id: string): any {
		const obj = localStorage.getItem(id);
		localStorage.removeItem(id);
		if (obj) return JSON.parse(obj);
	}

	static getObject(id: string): any {
		const obj = localStorage.getItem(id);
		if (obj) return JSON.parse(obj);
	}
}
