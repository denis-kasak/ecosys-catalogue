import cors from "cors";
import express from "express";
import helmet from "helmet";
import morgan from "morgan";
import booksApi from "./api/booksAPI";
import movieApi from "./api/movieAPI";
import MessageResponse from "./interfaces/MessageResponse";
import * as middlewares from "./middlewares";

const app = express();

app.use(morgan("dev"));
app.use(helmet());
app.use(cors());
app.use(express.json());

app.get<{}, MessageResponse>("/", (req, res) => {
  res.json({
    message: "🦄🌈✨👋🌎🌍🌏✨🌈🦄",
  });
});

app.use("/api/books", booksApi);
app.use("/api/movies", movieApi);

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

export default app;
