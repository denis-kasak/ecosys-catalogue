import { promises as fs } from "fs";
import { App } from "../app-types";

export class BookController {
  static async deleteOne(id: string) {
    let books = await getBooks();
    let index = books.findIndex((book) => book.id === id);
    if (index === -1) {
      throw new Error("Book not found");
    }
    books.splice(index, 1);
    await saveBooks(books);
  }

  static async updateOne(id: string, book: App.Book) {
    let books = await getBooks();
    let index = books.findIndex((book) => book.id === id);
    if (index === -1) {
      throw new Error("Book not found");
    }

    book.updatedAt = Date.now();
    books[index] = book;

    await saveBooks(books);

    return book;
  }
  static async createOne(book: App.Book) {
    book.id = Date.now().toString();
    book.updatedAt = Date.now();
    book.createdAt = Date.now();
    await saveBook(book);
    return book;
  }

  static async getOne(id: string) {
    let books = await getBooks();
    return books.find((book) => book.id === id);
  }

  static async getAll() {
    return await getBooks();
  }
}

async function getBooks(): Promise<App.Book[]> {
  try {
    let json = await fs.readFile("data/books.json", "utf-8");
    return JSON.parse(json) as App.Book[];
  } catch (error: unknown) {
    // Check if error has a code property and if it's 'ENOENT'
    if (
      typeof error === "object" &&
      error !== null &&
      "code" in error &&
      (error as { code: string }).code === "ENOENT"
    ) {
      await fs.writeFile("data/books.json", JSON.stringify([]), "utf-8");
      return [];
    } else {
      // If the error is not because the file doesn't exist or it's not an object with a 'code' property, throw it
      throw error;
    }
  }
}

async function saveBook(book: App.Book) {
  let books = await getBooks();
  books.push(book);
  await fs.writeFile("data/books.json", JSON.stringify(books), "utf-8");
}

async function saveBooks(books: App.Book[]) {
  await fs.writeFile("data/books.json", JSON.stringify(books), "utf-8");
}
