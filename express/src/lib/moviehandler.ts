import { promises as fs } from "fs";
import { App } from "../app-types";
import { MovieDb as MDB } from "moviedb-promise";
import MovieDB = require("moviedb-promise");

export class MovieController {
  static saveMovie(movie: any) {
    throw new Error("Method not implemented.");
  }
  static async queryFullMovie(id: string, r: { mdb: MDB }) {
    const basics = await r.mdb.movieInfo({ id });
    const credits = await r.mdb.movieCredits({ id });
    const images = await r.mdb.movieImages({ id });

    return { basics, credits, images };
  }

  static async deleteOne(id: string) {
    let movies = await getMovies();
    let index = movies.findIndex((movie) => movie.id === id);
    if (index === -1) {
      throw new Error("Movie not found");
    }
    movies.splice(index, 1);
    await saveMovies(movies);
  }

  static async updateOne(id: string, movie: App.Movie) {
    let movies = await getMovies();
    let index = movies.findIndex((movie) => movie.id === id);
    if (index === -1) {
      throw new Error("Movie not found");
    }

    movie.updatedAt = Date.now();
    movies[index] = movie;

    await saveMovies(movies);

    return movie;
  }
  static async createOne(movie: App.Movie) {
    movie.id = Date.now().toString();
    movie.updatedAt = Date.now();
    movie.createdAt = Date.now();
    await saveMovie(movie);
    return movie;
  }

  static async getOne(id: string) {
    let movies = await getMovies();
    return movies.find((movie) => movie.id === id);
  }

  static async getAll() {
    return await getMovies();
  }
}

async function getMovies(): Promise<App.Movie[]> {
  try {
    let json = await fs.readFile("data/movies.json", "utf-8");
    return JSON.parse(json) as App.Movie[];
  } catch (error: unknown) {
    // Check if error has a code property and if it's 'ENOENT'
    if (
      typeof error === "object" &&
      error !== null &&
      "code" in error &&
      (error as { code: string }).code === "ENOENT"
    ) {
      await fs.writeFile("data/movies.json", JSON.stringify([]), "utf-8");
      return [];
    } else {
      // If the error is not because the file doesn't exist or it's not an object with a 'code' property, throw it
      throw error;
    }
  }
}

async function saveMovie(movie: App.Movie) {
  let movies = await getMovies();
  movies.push(movie);
  await fs.writeFile("data/movies.json", JSON.stringify(movies), "utf-8");
}

async function saveMovies(movies: App.Movie[]) {
  await fs.writeFile("data/movies.json", JSON.stringify(movies), "utf-8");
}
