import MovieDB = require("moviedb-promise");

namespace App {
  export enum BookStatus {
    WANT_TO_READ = "want to read",
    READING = "reading",
    READ = "read",
  }

  export enum Feedback {
    LIKE = "like",
    DISLIKE = "dislike",
    NONE = "none",
  }

  export enum BookSource {
    GOOGLE_BOOKS = "Google Books API",
    CUSTOM = "custom",
  }

  export enum EditMode {
    EDIT = "edit",
    CREATE = "create",
    VIEW = "view",
  }

  export enum MovieSource {
    TMDB = "The Movie Database",
    CUSTOM = "custom",
  }

  export enum MovieStatus {
    WANT_TO_WATCH = "want to watch",
    WATCHING = "watching",
    WATCHED = "watched",
    ABANDONED = "abandoned",
  }

  export interface Media {
    id: string;
    source: MediaSource;
    createdAt: number;
    updatedAt: number;
    userInfo?: Impression;
    thumbnail: string;
    nsfw: boolean;
    title: string;
    subtitle: string;
    status?: string;
    description: string;
  }
  export interface Impression {
    feedback: Feedback;
    rating: number;
    review: string;
  }
  export interface ImageLinks {}

  export interface MediaSource {
    name: BookSource;
    id: string;
    selfLink: string;
  }

  export interface IndustryIdentifier {
    type?: string;
    identifier?: string;
  }
  export interface BookQuery {
    volumeid: string | undefined;
    query: string | undefined;
    intitle: string | undefined;
    inauthor: string | undefined;
    inpublisher: string | undefined;
    subject: string | undefined;
    isbn: string | undefined;
    lccn: string | undefined;
    oclc: string | undefined;
  }

  export interface Book extends Media {
    status: BookStatus;
    volumeInfo: {
      authors: string[];
      categories: string[];
      industryIdentifiers: IndustryIdentifier[];
      language: string;
      pageCount: number;
      publisher: string;
      publishedDate: string;
    };
  }

  export interface TMDB {}

  export interface MovieResponse {
    page: number;
    total_pages: number;
    total_results: number;
    results: MovieResult[];
  }

  export interface MovieResult {
    adult: boolean;
    backdrop_path: string;
    genre_ids: number[];
    id: number;
    original_language: string;
    original_title: string;
    overview: string;
    popularity: number;
    poster_path?: string;
    release_date: string;
    title: string;
    video: boolean;
    vote_average: number;
    vote_count: number;
  }

  export interface ShowResponse extends MovieResponse {}

  export interface ShowResult {
    adult: boolean;
    backdrop_path: string;
    genre_ids: number[];
    id: number;
    origin_country: string[];
    original_name: string;
    overview: string;
    popularity: number;
    poster_path?: string;
    first_air_date: string;
    name: string;
    vote_average: number;
    vote_count: number;
  }

  export interface MovieQuery {
    query: string;
    include_adult: boolean;
    language: string;
    primary_release_year: number;
    page: number;
    year: number;
  }

  export interface ShowQuery {
    query: string;
    first_air_date_year: number;
    include_adult: boolean;
    language: string;
    page: number;
    year: number;
  }

  export interface FullMovieResponse {
    basics: MovieDB.MovieResponse;
    credits: MovieDB.CreditsResponse;
    images: MovieDB.MovieImagesResponse;
  }

  export interface Movie extends Media {
    status: MovieStatus;
    movieInfo: {
      genres: string[];
      originalLanguage: string;
      originalTitle: string;
      productionCompanies: string[];
      productionCountries: string[];
      releaseDate: string;
      runtime: number;
      spokenLanguages: string[];
    };
  }
}

export { App };
