import express from "express";
import { MovieDb as MDB } from "moviedb-promise";
import dotenv from "dotenv";
import MovieDB = require("moviedb-promise");
import { MovieController } from "../lib/moviehandler";
import { App } from "../app-types";

dotenv.config();
const mdb = new MDB(process.env.MOVIE_DB_API_KEY || "");

const router = express.Router();

router.get("/extract", async (req, res) => {
  const params: unknown = req.query;
  try {
    if (req.query.id) {
      const movie = await MovieController.queryFullMovie(
        req.query.id.toString(),
        { mdb }
      );
      res.json(movie);
    } else {
      const movies = await mdb.searchMovie(
        params as MovieDB.SearchMovieRequest
      );
      res.json(movies);
    }
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
});

router.get("/extractfull", async (req, res) => {
  const { id } = req.query;
  try {
    if (!id) throw new Error("No id provided");
    const movie = await MovieController.queryFullMovie(id.toString(), { mdb });
    res.json(movie);
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
});

router.post("/", async (req, res) => {
  const movie = req.body as App.Movie;
  try {
    const result = await MovieController.createOne(movie);
    res.json(result);
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
});

router.get("/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const result = await MovieController.getOne(id);
    res.json(result);
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
});

router.put("/:id", async (req, res) => {
  const id = req.params.id;
  const movie = req.body as App.Movie;
  try {
    const result = await MovieController.updateOne(id, movie);
    res.json(result);
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
});

router.delete("/:id", async (req, res) => {
  const id = req.params.id;
  try {
    await MovieController.deleteOne(id);
    res.json({ message: "Movie deleted" });
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
});

router.get("/", async (req, res) => {
  try {
    const movies = await MovieController.getAll();
    res.json(movies);
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
});

export default router;
