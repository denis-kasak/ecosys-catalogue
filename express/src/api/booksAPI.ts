import express from "express";
import google from "googleapis";
import { google as g } from "googleapis";
import { BookController } from "../lib/bookhandler";
import { App } from "../app-types";

function buildQuery(query: App.BookQuery) {
  let q: string = query.query || "";

  if (query.intitle) q += ` intitle:${query.intitle}`;
  if (query.inauthor) q += ` inauthor:${query.inauthor}`;
  if (query.inpublisher) q += ` inpublisher:${query.inpublisher}`;
  if (query.subject) q += ` subject:${query.subject}`;
  if (query.isbn) q += ` isbc:${query.isbn}`;
  if (query.lccn) q += ` lccn:${query.lccn}`;
  if (query.oclc) q += ` oclc:${query.oclc}`;

  return q;
}

async function findBook(query: App.BookQuery) {
  return new Promise<google.books_v1.Schema$Volume[]>((resolve, reject) => {
    const books = g.books("v1");

    if (query.volumeid) {
      books.volumes
        .get({
          volumeId: query.volumeid,
          key: process.env.GOOGLE_API_KEY,
        })
        .then((res) => {
          if (res.data) resolve([res.data]);
        })
        .catch((err) => {
          reject(err);
        });
    } else {
      let q = buildQuery(query);
      if (!q) return resolve([]);
      books.volumes
        .list({
          q: q,
          key: process.env.GOOGLE_API_KEY,
          maxResults: 40,
        })
        .then((res) => {
          if (res.data.items) resolve(res.data.items);
          resolve([]);
        })
        .catch((err) => {
          reject(err);
        });
    }
  });
}

const router = express.Router();

router.get("/extract", async (req, res) => {
  const params: unknown = req.query;
  try {
    const books = await findBook(params as App.BookQuery);
    res.json(books);
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
});

router.post("/", async (req, res) => {
  const book = req.body as App.Book;
  try {
    const result = await BookController.createOne(book);
    res.json(result);
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
});

router.get("/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const result = await BookController.getOne(id);
    res.json(result);
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
});

router.put("/:id", async (req, res) => {
  const id = req.params.id;
  const book = req.body as App.Book;
  try {
    const result = await BookController.updateOne(id, book);
    res.json(result);
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
});

router.delete("/:id", async (req, res) => {
  const id = req.params.id;
  try {
    await BookController.deleteOne(id);
    res.json({ message: "Book deleted" });
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
});

router.get("/", async (req, res) => {
  try {
    const result = await BookController.getAll();
    res.json(result);
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
});

export default router;
