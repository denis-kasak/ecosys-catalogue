import dotenv from "dotenv";

if (!process.env.MOVIE_DB_API_KEY) throw new Error("Missing Movie DB API key");
